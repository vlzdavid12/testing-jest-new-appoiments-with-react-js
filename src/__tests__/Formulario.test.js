import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import Formulario from '../components/Formulario';
import '@testing-library/jest-dom/extend-expect';
import userEvent from "@testing-library/user-event";
//Function Space
const crearCita = jest.fn();

test('<Formulario> Loader Form Check Validation', () => {
        //Start testing Debugging
        //const wrapper = render(<Formulario />);
        //wrapper.debug();

       render(<Formulario crear={crearCita} />);
    const title =  screen.getByTestId('title');

       expect(title.tagName).toBe('H2');
       expect(title.textContent).toBe('Crear Cita');
       expect(title.textContent).not.toBe('New appointment');

        //Botom Submit Action
       const btnsubmit =  screen.getByTestId('btn-submit');
       expect(btnsubmit.tagName).toBe('BUTTON');
       expect(btnsubmit.textContent).toBe('Agregar Cita');
       expect(btnsubmit.textContent).not.toBe('Agregar Nueva Cita');

});

test('<Formulario /> Validation of formulario', () =>{
   render(<Formulario crear={crearCita} />);

   //Click Action Button
   const btnSubmit = screen.getByTestId('btn-submit');
   fireEvent.click(btnSubmit);

    //Check ALERT
    const alert = screen.getByTestId('alert');
    expect(alert).toBeInTheDocument();
    expect(alert.textContent).toBe('Todos los campos son obligatorios');
    expect(alert.tagName).toBe('P');
    expect(alert.tagName).not.toBe('BUTTON');

});

test('<Formulario /> Validación Formulario', ()=>{

    render(
        <Formulario crearCita={crearCita} />
    )

    const btnSubmit = screen.getByTestId('btn-submit');

    /*fireEvent.change(screen.getByTestId('txt-mascot'), {
        target: {value: 'Hook'}
    })*/

    //Util userEvent
    userEvent.type(screen.getByTestId('txt-mascot'), 'Hook');
    userEvent.type(screen.getByTestId('txt-property'), 'Propietario');
    userEvent.type(screen.getByTestId('txt-date'), '2021-09-10');
    userEvent.type(screen.getByTestId('txt-time'), '10:30');
    userEvent.type(screen.getByTestId('txt-symptom'), 'Solo Duerme');

    //fireEvent.click(btnSubmit);

    userEvent.click(btnSubmit);

    //Check ALert
    const alert = screen.queryByTestId('alert');
    expect( alert ).not.toBeInTheDocument();

    //Verify Call Function
    expect(crearCita).toHaveBeenCalled();
    expect(crearCita).toHaveBeenCalledTimes(1);



})
