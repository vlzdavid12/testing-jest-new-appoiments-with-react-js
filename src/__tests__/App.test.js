import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import userEvent from "@testing-library/user-event";
import App from "../App";

//Function Space
const crearCita = jest.fn();

test('<App /> Full Complete!',()=>{
    //const wrapper = render(<App />);
    //wrapper.debug();
    render(<App />);
    const btnSubmit = screen.getByTestId('btn-submit');
    const title =  screen.getByTestId('titleapp')
    expect(title.textContent).toBe('Administrador de Pacientes')
    expect(title.tagName).toBe('H1')
    const title2 =  screen.getByTestId('title')
    expect(title2.textContent).toBe('Crear Cita')

    expect(screen.getByText('Crear Cita')).toBeInTheDocument();
    expect(screen.getByText('Administrador de Pacientes')).toBeInTheDocument();

    //Action Submit
    userEvent.click(btnSubmit);

    const alert = screen.getByTestId('alert');
    expect(alert).toBeInTheDocument();

    expect(screen.getByTestId('title-dynamic').textContent).toBe('No hay citas')
    expect(screen.getByTestId('title-dynamic').textContent).not.toBe('Administra tus Citas')

    //Insert input txt
    userEvent.type(screen.getByTestId('txt-mascot'), 'Hook');
    userEvent.type(screen.getByTestId('txt-property'), 'Propietario');
    userEvent.type(screen.getByTestId('txt-date'), '2021-09-10');
    userEvent.type(screen.getByTestId('txt-time'), '10:30');
    userEvent.type(screen.getByTestId('txt-symptom'), 'Solo Duerme');

    //Action Submit
    userEvent.click(btnSubmit);

    expect(screen.getByTestId('title-dynamic').textContent).toBe('Administra tus Citas')


})

test('<App /> Verify Quaotas in Dom!', async ()=>{
    render(<App />);

    const quote = await screen.findAllByTestId('quote')

    //console.log(quote.toString())
    //Verify data send correctly - Create Archive
    //expect(quote).toMatchSnapshot();

    expect(screen.getByTestId('btn-delete').tagName).toBe('BUTTON');
    expect(screen.getByTestId('btn-delete')).toBeInTheDocument();

    expect(screen.getByText('Hook')).toBeInTheDocument();
});

test('<App /> Delete Quote',()=>{
    render(<App />);

    const btnDelete = screen.getByTestId('btn-delete');

    //Verify Ok!
    expect(btnDelete.tagName).toBe('BUTTON');
    expect(btnDelete).toBeInTheDocument();

    //Action Click
    userEvent.click(btnDelete);

    //Quote Not it should not be
    expect(btnDelete).not.toBeInTheDocument();
    expect(screen.queryByText('Hook')).not.toBeInTheDocument();
    expect(screen.queryByText('cita')).not.toBeInTheDocument();
})
